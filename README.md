# 🔍 Downstreamer

Keep track of upstream automatically.

## What's this?

Downstreamer aims to keep your Salsa project up to date.
Running on scheduled pipelines, it checks for new upstream versions.
When new versions are found, Downstreamer opens a Merge Request on your project and you decide if and when to merge it.


## How does it work?

Downstreamer uses Docker to create an empty container for each project on the list.

After cloning the repository, uscan is run to check for new upstream releases.
If a new version is found, a new branch is created with the name `upstream-update-{version}` and pushed using [git-buildpackage](https://github.com/agx/git-buildpackage).
This also updates the `upstream` and `pristine-tar` branches.

When the branches are pushed, a merge request is created on your project.

We recommend using the [Salsa CI Pipeline](https://salsa.debian.org/salsa-ci-team/pipeline) to keep your project in good shape :)

If any of this steps fail, Downstreamer will try to open an issue on your project to let you know about the problem.
No merge requests or projects will be opened if Downstreamer finds a previous merge request or issue still open.


## How do I add my project?
The first step is to give `salsa-pipeline-guest` developer permission.
This will allow the bot to push into the project and create merge requests.
No workaround has been found for this yet.

After this, submit a Merge Request to [repos.list](https://salsa.debian.org/salsa-ci-team/downstreamer/blob/master/repos.list) list.
When your submission is accepted, your project will be included on the checking the next time Downstreamer is run.

Currently the scheduled pipeline runs daily.


## Where is this run?

This script needs private tokens to run.
Because of this, it's not possible to run it on this same public project.

We've created another private project that clones this repository and runs it with the following script:

```bash
echo -e "machine salsa.debian.org\nlogin salsa-pipeline-guest\npassword ${SALSA_PIPELINE_PASSWORD}" > ~/.netrc
git clone https://salsa.debian.org/salsa-ci-team/downstreamer.git
cd downstreamer
python3 main.py
```

If you want to run it yourself, `SALSA_PIPELINE_PASSWORD` and `GITLAB_TOKEN` variables need to be set on your environment.
