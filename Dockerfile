from debian:unstable

ENV DEBIAN_FRONTEND noninteractive

RUN echo 'Acquire::HTTP::Proxy "http://192.168.0.240:3142";' >> /etc/apt/apt.conf.d/01proxy \
    && echo 'Acquire::HTTPS::Proxy "false";' >> /etc/apt/apt.conf.d/01proxy;

RUN apt-get update && apt-get install -y --no-install-recommends \
    eatmydata

RUN apt-get update && eatmydata apt-get install -y \
    git-buildpackage \
    ca-certificates \
    devscripts

RUN mkdir -p /root/.ssh && ssh-keyscan -t rsa salsa.debian.org > /root/.ssh/known_hosts

RUN git config --global user.name "Downstreamer"; git config --global user.email "salsa-ci-team@debian.org";
